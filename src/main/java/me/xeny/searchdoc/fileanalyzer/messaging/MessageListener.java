package me.xeny.searchdoc.fileanalyzer.messaging;

import me.xeny.searchdoc.messaging.impl.PrintCommand;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class MessageListener {
//    @KafkaListener(topics = {PrintCommand.Topic.NAME}, containerFactory = "singleFactory")
    public void consume(PrintCommand dto) {
        System.out.println(dto.getPayload().getText());
    }
}
