package me.xeny.searchdoc.fileanalyzer.util;

public class HashUtil {
    private static final int p = 31;
    private static final long m = Long.MAX_VALUE;

    public static long computeHash(String s) {
        long hash_value = 0;
        long p_pow = 1;
        for (char c : s.toCharArray()) {
            hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
            p_pow = (p_pow * p) % m;
        }
        return hash_value;
    }
}
