package me.xeny.searchdoc.fileanalyzer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "bigrams")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Bigram {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private Long hash;
    private Long count;
    private Double idx;

    @OneToMany(mappedBy = "bigram", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DocumentBinding> documentBindings;
}
