package me.xeny.searchdoc.fileanalyzer.service;

import lombok.RequiredArgsConstructor;
import me.xeny.searchdoc.fileanalyzer.service.AnalyticService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {
    private final AnalyticService analyticService;

    @GetMapping("/save/{text}")
    public void saveDocument(@PathVariable("text") String text) {
        analyticService.analyzeDocument(text);
    }
}
