package me.xeny.searchdoc.fileanalyzer.service.impl;

import lombok.RequiredArgsConstructor;
import me.xeny.searchdoc.fileanalyzer.model.Bigram;
import me.xeny.searchdoc.fileanalyzer.model.BigramBinding;
import me.xeny.searchdoc.fileanalyzer.model.Document;
import me.xeny.searchdoc.fileanalyzer.model.DocumentBinding;
import me.xeny.searchdoc.fileanalyzer.repository.BigramBindingRepository;
import me.xeny.searchdoc.fileanalyzer.repository.BigramRepository;
import me.xeny.searchdoc.fileanalyzer.repository.DocumentRepository;
import me.xeny.searchdoc.fileanalyzer.service.AnalyticService;
import me.xeny.searchdoc.fileanalyzer.util.HashUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AnalyticServiceImpl implements AnalyticService {
    private final DocumentRepository documentRepository;
    private final BigramRepository bigramRepository;
    private final BigramBindingRepository bigramBindingRepository;

    @Override
    public void analyzeDocument(String text) {
        UUID id = UUID.randomUUID();
        Document document = Document.builder()
                .id(id)
                .bigramCount(0L)
                .bigramBindings(new ArrayList<>())
                .build();
        documentRepository.save(document);

        document = Document.builder()
                .id(id)
                .bigramCount(0L)
                .bigramBindings(new ArrayList<>())
                .build();

        getBigrams(text, document);

        documentRepository.save(document);
    }

    private void getBigrams(String text, Document document) {
        String[] words = text.split("\\.");
        for (int i = 0; i < words.length - 1; i++) {
            String bigramText = words[i] + ' ' + words[i + 1];
            long hash = UUID.nameUUIDFromBytes(bigramText.getBytes()).getMostSignificantBits();
            Bigram bigram = Bigram.builder()
                    .id(UUID.randomUUID())
                    .hash(hash)
                    .count(0L)
                    .idx(0D)
                    .documentBindings(new ArrayList<>())
                    .build();

            BigramBinding bigramBinding = BigramBinding.builder()
                    .id(UUID.randomUUID())
                    .document(document)
                    .bigram(bigram)
                    .build();

            DocumentBinding documentBinding = DocumentBinding.builder()
                    .id(UUID.randomUUID())
                    .bigram(bigram)
                    .document(document)
                    .build();

            document.getBigramBindings().add(bigramBinding);
            bigram.getDocumentBindings().add(documentBinding);
        }
    }
}
