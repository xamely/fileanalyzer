package me.xeny.searchdoc.fileanalyzer.service;

public interface AnalyticService {
    void analyzeDocument(String text);
}
