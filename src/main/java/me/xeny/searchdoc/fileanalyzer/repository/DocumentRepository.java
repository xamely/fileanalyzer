package me.xeny.searchdoc.fileanalyzer.repository;

import me.xeny.searchdoc.fileanalyzer.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DocumentRepository extends JpaRepository<Document, UUID> {
}
