package me.xeny.searchdoc.fileanalyzer.repository;

import me.xeny.searchdoc.fileanalyzer.model.BigramBinding;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BigramBindingRepository extends JpaRepository<BigramBinding, UUID> {
}
