package me.xeny.searchdoc.fileanalyzer.repository;

import me.xeny.searchdoc.fileanalyzer.model.Bigram;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BigramRepository extends JpaRepository<Bigram, UUID> {
}
